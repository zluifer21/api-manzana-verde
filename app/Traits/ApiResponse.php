<?php


namespace App\Traits;
use Illuminate\Http\Response;
use \Illuminate\Http\JsonResponse;
trait  ApiResponse
{
    public function successResponse($message = '', $content = null, $code = Response::HTTP_OK): JsonResponse{
        return $this->loadDataResponse($code, $message, $content);
    }

    public function errorResponse($message, $code, $content = null): JsonResponse{
        return $this->loadDataResponse($code, $message, $content);
    }

    private function loadDataResponse($code, $message, $content): JsonResponse
    {
        $response = response()->json([
            'message'=>$message,
            'data'=>$content
        ], $code);
        return $response;
    }

}
