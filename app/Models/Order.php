<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $attributes = ['code'=>''];
    protected $fillable = [
        'user_id',
        'code'
    ];
    protected $casts = [
        'created_at'  => 'date:Y-m-d',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->attributes['code'] = Str::uuid();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class, 'order_foods')->withPivot('quantity');
    }

}
