<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;
    protected $table = 'foods';
    protected $fillable = [
        'name',
        'photo',
        'description'
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class,'order_foods')->withPivot('quantity');
    }
}
