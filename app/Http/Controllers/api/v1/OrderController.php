<?php

namespace App\Http\Controllers\api\v1;

use App\Entities\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\StoreOrder;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use App\Services\Order\OrderService;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpStatus;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    use ApiResponse;
    /**
     * OrderController constructor.
     */
    private $orderService;
    private $message;
    public function __construct(OrderService $orderService,Message $message)
    {
        $this->orderService = $orderService;
        $this->message = $message;

    }

    public function index()
    {
        try {
            return $this->successResponse($this->message->data_list,$this->orderService->all(['user_id' => auth()->user()->id]));
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $this->errorResponse($this->message->default_error,HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function store(StoreOrder $request)
    {
        DB::beginTransaction();
        try {
            $this->orderService->save($request->all());
            DB::commit();
            return $this->successResponse($this->message->success_register,[],HttpStatus::HTTP_CREATED);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
            DB::rollBack();
            return $this->errorResponse($this->message->error_register,HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
