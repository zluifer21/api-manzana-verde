<?php

namespace App\Http\Controllers\api\v1;

use App\Entities\Message;
use App\Http\Controllers\Controller;
use App\Services\Food\FoodService;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpStatus;

class FoodController extends Controller
{
    use ApiResponse;
    /**
     * OrderController constructor.
     */
    private $foodService;
    private $message;
    public function __construct(FoodService $foodService,Message $message)
    {
        $this->foodService=$foodService;
        $this->message=$message;
    }

    public function index()
    {
        try {
            $object = $this->foodService->all();
            return $this->successResponse($this->message->data_list,$object);
        } catch (\Throwable $th) {
            return $this->errorResponse($this->message->default_error,HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
