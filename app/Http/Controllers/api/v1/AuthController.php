<?php

namespace App\Http\Controllers\api\v1;

use App\Entities\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AuthUser;
use App\Services\Auth\AuthService;
use App\Services\User\UserService;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpStatus;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    private $authService;
    private $message;

    use ApiResponse;
    public function __construct(AuthService $authService,Message $message)
    {
        $this->middleware('auth:api', ['except' => ['login']]);
        $this->authService=$authService;
        $this->message=$message;

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AuthUser $request)
    {
        try {
            $object =$this->authService->login($request->only(['email','password']));
            return $object?:$this->errorResponse( $this->message->error_login,HttpStatus::HTTP_UNAUTHORIZED);
        }catch (\Throwable $th) {
        return $this->errorResponse($this->message->error_register,HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

}
