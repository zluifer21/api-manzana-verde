<?php

namespace App\Http\Controllers\api\v1;

use App\Entities\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Services\User\UserService;
use Illuminate\Http\Response as HttpStatus;
use \App\Traits\ApiResponse;
use JWTAuth;

class UserController extends Controller
{
    use ApiResponse;
    /**
     * UserController constructor.
     */
    private $userService;
    private $message;
    public function __construct(UserService $userService,Message $message)
    {
        $this->userService=$userService;
        $this->message=$message;
    }

    /**
     * @throws \Throwable
     */
    public function store(StoreUser $request)
    {
        try {
            $object = $this->userService->save($request->all());
            $object->access_token=JWTAuth::fromUser($object);
            return $this->successResponse($this->message->success_register,$object,HttpStatus::HTTP_CREATED);
        }catch (\Throwable $th) {

            return $this->errorResponse($this->message->error_register,HttpStatus::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
