<?php

namespace App\Http\Requests\Auth;

use App\Entities\Message;
use Illuminate\Foundation\Http\FormRequest;


class AuthUser extends FormRequest
{
    /*
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'max:30', 'email'],
            'password' => ['required', 'max:30']
        ];
    }

    public function messages()
    {
        $message = new Message();
        return [
            'email.required' => $message->empty_field,
            'email.max' => $message->error_length_field,
            'email.email' => $message->error_email,
            'password.required' => $message->empty_field,
            'password.max' => $message->error_length_field,
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'e-mail',
            'password' => 'contraseña'
        ];
    }
}
