<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Entities\Message;
use Illuminate\Validation\Rule;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array('required', 'max:100'),
            'last_name' => array('required', 'max:100'),
            'email' => array('required', 'max:120', 'email', Rule::unique('users')->whereNull('deleted_at')),
            'password' => array('required', 'max:30'),
        ];
    }

    public function messages()
    {
        $message = new Message();
        return array(
            'name.required' => $message->empty_field,
            'name.max' => $message->error_length_field,
            'last_name.max' => $message->error_length_field,
            'email.required' => $message->empty_field,
            'email.max' => $message->error_length_field,
            'email.email' => $message->error_email,
            'email.unique' => $message->error_unique_male,
            'password.required' => $message->empty_field,
            'password.max' => $message->error_length_field,
        );
    }

    public function attributes()
    {
        return array(
            'name' => 'nombre',
            'last_name' => 'apellido',
            'email' => 'e-mail',
            'password' => 'contraseña',
        );
    }

}
