<?php

namespace App\Http\Requests\Order;

use App\Entities\Message;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orders'=>"required|array",
            'orders.*.food_id' => ['required','exists:foods,id'],
            'orders.*.quantity' => ['required','integer','gt:0']

        ];
    }

    public function messages()
    {
        $message = new Message();
        return array(
            'orders.required' => $message->empty_field,
            'orders.*.food_id.required' => $message->empty_field,
            'orders.*.food_id.exists' => str_replace(['{model}'],['Plato'],$message->error_relation),
            'orders.*.quantity.required' => $message->empty_field,
            'orders.*.quantity.integer' => $message->error_field_integer,
            'orders.*.quantity.gt' => $message->error_min_length_field,

        );
    }

    public function attributes()
    {
        return array(
            'orders' => 'Ornden',
        );
    }
}
