<?php


namespace App\Entities;


class Message
{
    public $empty_values = "Verifique que haya diligenciado todos los campos.";
    public $success_register = "Datos almacenados exitosamente.";
    public $error_register = "Ha ocurrido un error al registrar la información, por favor intente nuevamente.";
    public $success_update = "Datos actualizados exitosamente.";
    public $success_search = "Busqueda Exitosa.";
    public $error_update = "Ha ocurrido un error al actualizar la información, por favor intente nuevamente.";
    public $success_delete = "Datos eliminados exitosamente.";
    public $error_delete = "Ha ocurrido un error al eliminar la información, por favor intente nuevamente.";
    public $error_query = "No se encuentran datos almacenados en el sistema.";
    public $empty_field = "Verifique que haya diligenciado el campo :attribute.";
    public $error_length_field = "Verifique que el campo :attribute no exceda los :max caracteres permitidos";
    public $error_min_length_field = "Verifique que el campo :attribute tenga :min caracteres requeridos";
    public $error_email = "Verifique que el :attribute diligenciado sea una dirección de correo válida";
    public $error_relation = "El {modelo}  no existe";
    public $error_exists_male = "El :attribute seleccionado no existe.";
    public $error_exists_female = "La :attribute seleccionada no existe.";
    public $error_unique_male = "El :attribute introducido ya existe.";
    public $error_unique_female = "La :attribute diligenciada ya existe.";
    public $error_date = "Verifique que el campo :attribute sea una fecha válida";
    public $error_field_array = 'El parametro que corresponde a <<<:attribute>>> debe ser un Array';
    public $error_field_string = 'El parametro que corresponde a <<<:attribute>>> debe ser un String';
    public $error_field_numeric = 'El parametro que corresponde a <<<:attribute>>> debe ser un Entero';
    public $error_field_boolean = 'El parametro que corresponde a <<<:attribute>>> debe ser un Booleano';
    public $error_field_integer = 'El parametro que corresponde a <<<:attribute>>> debe ser un Entero';
    public $error_field_positive_integer = 'El parametro que corresponde a <<<:attribute>>> debe ser un Entero Positivo';
    public $error_field_number_min = "Verifique que el campo <<<:attribute>>> tenga un valor no menor a :min";
    public $error_field_number_max = "Verifique que el campo <<<:attribute>>> etnga un valor no mayor a :max";
    public $error_file_upload ="Error en la carga de archivo";
    public $delete_success = "Elemento eliminado exitosamente";
    public $default_error= "Ha odurrido un error inesperado";
    public $data_list= "Lista de datos";
    public $data_no_found= "datos no encontrados";
    public $error_login = "No se pudo iniciar sesión, contraseña o usario incorrectos";
}
