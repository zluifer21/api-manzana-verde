<?php


namespace App\Services;
use Illuminate\Database\Eloquent\Model;

class BaseService
{
    protected $model;
    private $relations;

    public function __construct(Model $model, array $relations=[] )
    {
        $this->model= $model;
        $this->relations =$relations;
    }

    public function all(array $search = [] )
    {
        $query = $this->model;
        if(!empty($this->relations)) $query=$query->with($this->relations);
        if (count($search)) {
            foreach ($search as $key => $value) {
                $query->where($key, $value);
            }
        }
        return $query->get();
    }

    public function save($data)
    {
        $object=$this->model->create($data);
        $object->save();
        return $object;
    }

}
