<?php


namespace App\Services\User;
use App\Models\User;
use App\Services\BaseService;

class UserService extends BaseService
{

    /**
     * UserService constructor.
     */
    public function __construct( User $model)
    {
        parent::__construct($model);
    }
}
