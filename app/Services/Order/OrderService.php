<?php


namespace App\Services\Order;


use App\Models\Order;
use App\Services\BaseService;

class OrderService extends BaseService
{

    /**
     * OrderService constructor.
     */
    const RELATIONS = ['foods'];
    public function __construct(Order $order)
    {
        parent::__construct($order,self::RELATIONS);
    }

    public function save($data)
    {
        auth()->user()->orders()->save(new Order())->foods()->attach($data['orders']);
    }
}
