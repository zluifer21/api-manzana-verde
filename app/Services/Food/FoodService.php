<?php


namespace App\Services\Food;


use App\Models\Food;
use App\Services\BaseService;

class FoodService extends BaseService
{

    /**
     * FoodService constructor.
     */
    public function __construct( Food $food)
    {
        parent::__construct($food);
    }
}
