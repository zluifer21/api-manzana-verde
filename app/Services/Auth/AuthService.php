<?php


namespace App\Services\Auth;
use Illuminate\Http\JsonResponse;

class AuthService
{
    public function login($credentials)
    {
        if (! $token = auth()->attempt($credentials)) {
            return false;
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
