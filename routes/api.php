<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1\{
    AuthController,
    UserController,
    FoodController,
    OrderController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class,'login']);
        Route::post('logout', [AuthController::class,'logout']);
    });
    Route::prefix('user')->group(function (){
        Route::post('register',[UserController::class,'store']);
    });
    Route::group(['middleware' => 'auth:api'], function (){
        Route::get('foods',[FoodController::class,'index']);
        Route::apiResources(['orders'=>OrderController::class],['only'=>['index','store']]);
    });
});
