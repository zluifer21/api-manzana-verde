<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foods')->insert([
            [
                'name' => 'Filete de pescado al limón',
                'photo' => 'https://manzanaverde.la/images/home/pe/plate-5.png',
                'description' => '600 kcal',
            ],
            [
                'name' => 'Lo mein de pollo',
                'photo' => 'https://manzanaverde.la/images/home/pe/plate-6.png',
                'description' => '600 Kcal',
            ],
            [
                'name' => 'Pancake de chocolate',
                'photo' => 'https://manzanaverde.la/images/home/pe/plate-7.png',
                'description' => '200 Kcal',
            ],
            [
                'name' => 'Sandwich de pollo',
                'photo' => 'https://manzanaverde.la/images/home/pe/plate-8.png',
                'description' => '200 Kcal',
            ]
        ]);
    }
}
